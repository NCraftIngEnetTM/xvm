"""
SPDX-License-Identifier: GPL-3.0-or-later
Copyright (c) 2013-2024 XVM Contributors
"""

from xfw import IS_DEVELOPMENT

if IS_DEVELOPMENT:
    import profiler
